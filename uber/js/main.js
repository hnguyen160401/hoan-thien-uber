const UBER_CAR = "uberCar";
const UBER_SUV = "uberSUV";
const UBER_BLACK = "uberBlack";
function tinhGiaTienKmDauTien(car) {
  if (car == UBER_CAR) {
    return 8000;
  }

  if (car == UBER_BLACK) {
    return 10000;
  }

  if (car == UBER_SUV) {
    return 9000;
  }
}

function tinhGiaTienKm1_19(car) {
  switch (car) {
    case UBER_CAR: {
      return 7500;
    }
    case UBER_SUV: {
      return 8500;
    }
    case UBER_BLACK: {
      return 9500;
    }
    default:
      return 0;
  }
}

function tinhGiaTienKm19TroDi(car) {
  switch (car) {
    case UBER_CAR: {
      return 7000;
    }
    case UBER_SUV: {
      return 8000;
    }
    case UBER_BLACK: {
      return 9000;
    }
    default:
      return 0;
  }
}

function tinhTienUberCar() {}

// main function
function tinhTienUber() {
  //   console.log("yes");
  var carOption = document.querySelector(
    'input[name="selector"]:checked'
  ).value;
  console.log("carOption: " + carOption);

  var giaGiaTienKmDauTien = tinhGiaTienKmDauTien(carOption);
  console.log("giaGiaTienKmDauTien", giaGiaTienKmDauTien);

  var giaGiaTienKm1_19 = tinhGiaTienKm1_19(carOption);
  console.log("giaGiaTienKm1_19", giaGiaTienKm1_19);

  var giaGiaTienKm19TroDi = tinhGiaTienKm19TroDi(carOption);
  console.log("giaGiaTienKm19TroDi", giaGiaTienKm19TroDi);

  var km = document.getElementById("txt-km").value * 1;
  if (km <= 1) {
    var tinhGiaTienKmDauTien = giaGiaTienKmDauTien * km;
    return tinhGiaTienKmDauTien;
    // console.log("tinhGiaTienKmDauTien");
  } else if (km < 1 && km <= 19) {
    var tinhGiaTienKm1_19 = giaGiaTienKmDauTien + (km - 1) * giaGiaTienKm1_19;
    return tinhGiaTienKm1_19;
  } else {
    var tinhGiaTienKm19TroDi =
      giaGiaTienKmDauTien +
      18 * giaGiaTienKm1_19 +
      (km - 19) * giaGiaTienKm19TroDi;
    return tinhGiaTienKm19TroDi;
  }
}
