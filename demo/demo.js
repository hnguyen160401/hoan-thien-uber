// ham khong co tham so
function sayHello() {
  console.log("hi");
  console.log("nice to meet u");
}

sayHello();
sayHello();
sayHello();

// ham co tham so
// username o day la parameter
function sayHi(username) {
  console.log("hi" + username);
}
// username o day la variable(bien)
var username = "alice";

sayHi("alice");
sayHi("bob");

function tinhDTB(toan, van) {
  var dtb = (toan + van) / 2;
  console.log("dtb: ", dtb);
  // ham co gia tri tra ve
  return dtb;
}

tinhDTB(2, 4);
// hoac
var diemToan = 2;
var diemVan = 4;
tinhDTB(diemToan, diemVan);
var score = tinhDTB(diemToan, diemVan);
console.log("score:", score);

// cach goi ham khac
var saGoodBye = function () {
  console.log("good bye");
};

saGoodBye();
